import pandas as pd
import numpy as np
import operator

class Product:
    def __init__(self):
        self._data = self.search()
        self._eans = self.__set_eans()

    def search(self):
        return pd.read_csv("products.csv", sep=";")

    def __set_eans(self):
        return self._data['ean'].drop_duplicates()
    
    def search_by_ean(self, ean):
        result = self._data.loc[self._data['ean'] == ean]
        return result
    
    # 
    def _min_price(self, ean):
        product = self.search_by_ean(ean)
        prices = self.__set_prices(product)
        return min(prices)
    
    def _max_price(self, ean):
        product = self.search_by_ean(ean)
        prices = self.__set_prices(product)
        return max(prices)

    def _avg_price(self, ean):
        product = self.search_by_ean(ean)
        prices = self.__set_prices(product)
        return sum(prices)/len(prices)

    def _mode_price(self, ean):
        product = self.search_by_ean(ean)
        prices = self.__set_prices(product)
        self.prices = prices
        prices_list = prices.to_list()
        return self.__get_mode(prices_list)

    def _median_price(self, ean):
        product = self.search_by_ean(ean)
        prices = self.__set_prices(product)
        array = np.sort(prices.to_numpy())
        median = self.__get_median(array)
        return median

    def exporter(self): 
        data = self.analysis()
        data.to_csv(path_or_buf="data.csv", sep=";", index=False)

    # 
    def __get_mode(self, array):
        str_array = list(map(str, array))
        unique_prices = dict.fromkeys(str_array)
        for i in unique_prices:
            unique_prices[i] = array.count(float(i))
        return float(max(unique_prices.items(), key=operator.itemgetter(1))[0])

    def __get_median(self, array):
        if not len(array) % 2:
            return self.__median_even(array)
        else:
            return self.__median_odd(array)

    def __median_even(self, array): 
        index = int(len(array)/2)
        median1, median2 = array[index-1:index+1]
        return (median1 + median2) / 2
    
    def __median_odd(self, array):
        index = int(len(array)/2)
        median = array[index]
        return median
  
    def _get_name_by_ean(self, ean):
        product = self.search_by_ean(ean)
        return self.__set_name(product)

    def __set_name(self, data):
        return data['produto'].iloc[0]

    def __set_prices(self, data):
        return data['preco_por']

    def __set_columns_to_export(self):
        columns = [
            "produto",
            "ean",
            "min_price",
            "max_price",
            "avg_price",
            "mode_price",
            "median_price"]
        return columns

    def analysis(self):
        exportable = pd.DataFrame(columns=self.__set_columns_to_export())
        for _, ean in self._eans.items():
            per_ean = [
                self._get_name_by_ean(ean),
                ean,
                self._min_price(ean),
                self._max_price(ean),
                self._avg_price(ean),
                self._mode_price(ean),
                self._median_price(ean)]
            to_append = pd.DataFrame([per_ean], columns=self.__set_columns_to_export())
            exportable = exportable.append(to_append)
        return exportable