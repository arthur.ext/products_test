from flask import Blueprint, request
from api import app
from features.product.product import Product

api = Blueprint('route', __name__, url_prefix='/api')

def get_ean(ean, data):
    result = data.loc[data['ean'] == int(ean)]
    return result

# routes
@api.route('/search_by_ean/<ean>', methods=['GET'])
def search_by_ean(ean):
    p = Product()
    data = p.analysis()
    body_ean = get_ean(ean, data)

    result = body_ean.to_dict(orient="split")['data'][0]
    response = {"products": {
        ean: {
            "nome": result[0],
            "min_price": result[2],
            "max_price": result[3],
            "avg_price": result[4],
            "mode_price": result[5],
            "median_price": result[6]
        }
    }}
    return response, 200

@api.route('/export', methods=['GET'])
def export():
    p = Product()
    p.exporter()

    return "Analysis exported to data.csv file", 201