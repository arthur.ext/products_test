from flask import Flask

app = Flask(__name__)

from .routes import api
app.register_blueprint(api)